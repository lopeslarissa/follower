# Follower #

Aplicação web para monitorar hashtags no Twitter 

[Servidor Web](https://appfollower.herokuapp.com/)

## Instalação ##

* ``` git clone https://lopeslarissa@bitbucket.org/lopeslarissa/follower.git ```
* ``` pip install -r requirements.txt ```
* ``` python manage.py make migrations ```
* ``` python manage.py make migrate ```
* ``` python manage.py runserver ```

## Buscar tweets ##

1. Cadastre as hashtags que serão monitoradas 
2. Execute no terminal: ``` python manage.py runcrons ```
3. Para buscar automaticamente os tweets é preciso agendar a tarefa no servidor