from django.contrib import admin
from appFollower.models.hashtag import Hashtag
from appFollower.models.tweet import Tweet

admin.site.register(Hashtag)
admin.site.register(Tweet)