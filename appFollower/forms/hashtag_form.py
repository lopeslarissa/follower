from django.forms import ModelForm, CharField
from appFollower.models.hashtag import Hashtag


class HashtagForm(ModelForm):
    nome = CharField(label="Hashatag", required=True)

    class Meta:
        model = Hashtag
        fields = ('nome',)
