from django.db import models


class Hashtag(models.Model):
    nome = models.CharField(max_length=250)

    class Meta:
        verbose_name_plural = 'Hashtags'

    def __str__(self):
        return '%s' % self.nome
