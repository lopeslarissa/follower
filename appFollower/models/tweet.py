from django.db import models
from django.urls import reverse_lazy
from appFollower.models.hashtag import Hashtag


class Tweet(models.Model):
    texto = models.CharField(max_length=280)
    data_pub = models.DateField()
    usuario = models.CharField(max_length=100)
    hashtag = models.ForeignKey(Hashtag, on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Tweets'
        ordering = ['-id']

    def __str__(self):
        return '%s' % self.texto
