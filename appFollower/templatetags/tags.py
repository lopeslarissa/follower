from django import template


register = template.Library()
@register.simple_tag(takes_context=True)
def get_param(context, **kwargs):
    d = context['request'].GET.copy()
    for k, v in kwargs.items():
        if v:
            d[k] = v
    return d.urlencode()
