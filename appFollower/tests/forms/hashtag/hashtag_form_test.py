from django.test import TestCase
from appFollower.forms.hashtag_form import HashtagForm


class HashtagFormTest(TestCase):

    def test_valid_form(self):
        form = HashtagForm({'nome': u'#iphone'})
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form = HashtagForm({'nome': u''})
        self.assertFalse(form.is_valid())

