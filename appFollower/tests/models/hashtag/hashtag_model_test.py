from django.test import TestCase
from appFollower.models.hashtag import Hashtag


class HashtagModelTest(TestCase):

    def create_hashtag(self, nome=u'#linux'):
        return Hashtag.objects.create(nome=nome)

    def test_hashtag_creation(self):
        hashtag = self.create_hashtag()
        self.assertTrue(isinstance(hashtag, Hashtag))
        self.assertEqual(hashtag.__unicode__(), hashtag.nome)
