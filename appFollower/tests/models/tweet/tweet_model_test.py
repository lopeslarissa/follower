from django.test import TestCase
from appFollower.models.hashtag import Hashtag
from appFollower.models.tweet import Tweet


class TweetModelTest(TestCase):

    def create_tweet(self, texto=u'tweet sobre linux', usuario='linux_s2', data_pub='2019-05-20', hashtag='linux'):
        hashtag = Hashtag.objects.create(nome=hashtag)
        return Tweet.objects.create(texto=texto, usuario=usuario, data_pub=data_pub, hashtag=hashtag)

    def test_tweet_creation(self):
        tweet = self.create_tweet()
        self.assertTrue(isinstance(tweet, Tweet))
        self.assertEqual(tweet.__unicode__(), tweet.texto)
