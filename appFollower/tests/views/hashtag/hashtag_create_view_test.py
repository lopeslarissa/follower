from django.test import TestCase
from django.urls import reverse


class HashtagCreateViewTest(TestCase):

    def test_hashtag_create_url(self):
        url = reverse("hashtag-create")
        self.response = self.client.get(url)
        self.assertEqual(self.response.status_code, 200)

    def test_hashtag_create_success(self):
        url = reverse("hashtag-create")
        self.response = self.client.post(url, data={"nome": "#linux"})
        self.assertEqual(self.response.status_code, 302)





