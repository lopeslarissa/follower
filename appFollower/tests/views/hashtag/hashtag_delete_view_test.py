from django.test import TestCase
from django.urls import reverse
from appFollower.models.hashtag import Hashtag


class HashtagDeleteViewTest(TestCase):

    def setUp(self):
        Hashtag.objects.create(nome="#twitter")

    def test_hashtag_delete_success(self):
        hashtag = Hashtag.objects.get(nome="#twitter").pk
        url = reverse("hashtag-delete", kwargs={'pk': hashtag})
        self.response = self.client.post(url, {'pk': hashtag})
        self.assertEqual(self.response.status_code, 302)

    def test_hashtag_delete_url(self):
        hashtag = Hashtag.objects.get(nome="#twitter").pk
        url = reverse("hashtag-delete", kwargs={'pk': hashtag})
        self.response = self.client.get(url)
        self.assertEqual(self.response.status_code, 200)