from django.test import TestCase
from django.urls import reverse


class HashtagListViewTest(TestCase):

    def test_hashtag_list_url(self):
        url = reverse("hashtag-list")
        self.response = self.client.get(url)
        self.assertEqual(self.response.status_code, 200)