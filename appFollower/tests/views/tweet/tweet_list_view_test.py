from django.test import TestCase
from django.urls import reverse


class TweetListViewTest(TestCase):

    def test_tweet_list_url(self):
        url = reverse("tweet-list")
        self.response = self.client.get(url)
        self.assertEqual(self.response.status_code, 200)