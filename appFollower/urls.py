from django.urls import path
from appFollower.views.hashtag.hashtag_create_view import HashtagCreateView
from appFollower.views.hashtag.hashtag_delete_view import HashtagDeleteView
from appFollower.views.hashtag.hashtag_list_view import HashtagListView
from appFollower.views.tweet.tweet_list_view import TweetListView

urlpatterns = [
    path(r'hashtag/list/', HashtagListView.as_view(), name='hashtag-list'),
    path(r'hashtag/create/', HashtagCreateView.as_view(), name='hashtag-create'),
    path(r'hashtag/<int:pk>/delete/', HashtagDeleteView.as_view(), name='hashtag-delete'),
    path(r'tweet/list/', TweetListView.as_view(), name='tweet-list'),
]
