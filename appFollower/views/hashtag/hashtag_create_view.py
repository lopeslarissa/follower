from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy
from django.views.generic import CreateView
from appFollower.forms.hashtag_form import HashtagForm
from appFollower.models.hashtag import Hashtag


class HashtagCreateView(SuccessMessageMixin, CreateView):
    model = Hashtag
    form_class = HashtagForm
    success_message = gettext_lazy('Hashtag cadastrada com sucesso')
    template_name = "appFollower/hashtag_list.html"
    success_url = reverse_lazy('hashtag-list')

