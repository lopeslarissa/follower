from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy
from django.views.generic import DeleteView
from appFollower.models.hashtag import Hashtag


class HashtagDeleteView(SuccessMessageMixin, DeleteView):
    queryset = Hashtag.objects.all()
    success_url = reverse_lazy('hashtag-list')
    success_message = gettext_lazy('Hashtag excluída com sucesso')
    template_name = "appFollower/hashtag_list.html"

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(HashtagDeleteView, self).delete(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.post(self, request, *args, **kwargs)