from django.views.generic import ListView
from appFollower.models.hashtag import Hashtag


class HashtagListView(ListView):
    queryset = Hashtag.objects.all()
    paginate_by = 10
