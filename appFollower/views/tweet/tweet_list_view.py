from django.views.generic import ListView
from appFollower.models.tweet import Tweet


class TweetListView(ListView):
    model = Tweet
    paginate_by = 10

    def get_queryset(self):
        return Tweet.objects.filter(hashtag__nome__icontains=self.request.GET.get('hashtag', ""))



