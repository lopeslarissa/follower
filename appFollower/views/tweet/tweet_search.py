import tweepy as tw
from datetime import date, timedelta
from appFollower.models.hashtag import Hashtag
from appFollower.models.tweet import Tweet
from follower.settings import CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET


def tweet_search():
    auth = tw.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tw.API(auth, wait_on_rate_limit=True)

    count = 0
    for hashtag in Hashtag.objects.all():
        queryset = tw.Cursor(api.search,
                             q=hashtag.nome,
                             since=date.today() - timedelta(7),
                             count=100).items(450)

        for tweet in queryset:
            count += 1
            Tweet.objects.create(texto=tweet.text, usuario=tweet.user.screen_name, data_pub=tweet.created_at,
                                 hashtag=hashtag)

    return print("%d tweets salvos" % count)