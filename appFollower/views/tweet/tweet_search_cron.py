from django_cron import CronJobBase, Schedule
from appFollower.views.tweet.tweet_search import tweet_search


class TweetSearchCron(CronJobBase):
    RUN_EVERY_MINS = 15

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = "appFollower.views.tweet.tweet_search.tweet_search"

    def do(self):
        tweet_search()